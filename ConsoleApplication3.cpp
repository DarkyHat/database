#include "pch.h"
#include <locale.h>
#include <fstream>
#include <string>
#include <iostream>
#include <cstdio>
#include <windows.h>
using namespace std;



struct stud{ //Создаем структуру
	char first_name[15], last_name[20];
	int year, month, day;
	double av_grade;
};

int cnt = 0; //счетчик
int i = 0; // счетчик для массива
stud *buff = new stud[20];// создание массива данных


void show_student() { // функция показать информацию об 1 студенте. 
	cout << "Студент номер " << cnt + 1 << endl; // зацикливается в функции show_all_base
	cout << "Фамилия: " << buff[cnt].last_name << endl; // и вызывается несколько раз
	cout << "Имя: " << buff[cnt].first_name << endl; // тем самым показывает всех студентов
	cout << "Год рождения: " << buff[cnt].year << endl;
	cout << "Месяц рождения: " << buff[cnt].month << endl;
	cout << "День рождения: " << buff[cnt].day << endl;
	cout << "Средний балл: " << buff[cnt].av_grade << endl << endl;
}


void reading() { // функция чтения из файла
	string filename; 
	printf("Введите имя файла\n");
	cin >> filename;
	ifstream file; //создаем объект файла для чтения
	file.open("C:\\Users\\" + filename);
	if (!file){
		cout << "Файл не открыт\n\n";
	}
	else
	{
		cout << "Файл открыт!\n\n";
		while(1){ //записываем из файла в массив
			file >> buff[i].last_name;
			file >> buff[i].first_name;
			file >> buff[i].year;
			file >> buff[i].month;
			file >> buff[i].day;
			file >> buff[i].av_grade;
			i++;
			if (file.eof()) { // пока не конец файла
				break; //выходим из цикла
			}
		}
		printf("Считывание завершено\n");
		file.close();
	}
	system("pause");
}

void out_put_to_file() { // запись в файл
	string name;
	printf("Введите имя файла\n");
	cin >> name;
	ofstream file2; //создаем объект файла для записи 
	file2.open("C:\\Users\\" + name);
	if (!file2) {
		cout << "Файл не открыт\n\n";
	}
	else{
		cout << "Файл открыт\n\n";
		for (cnt = 0; cnt < i; cnt++) { //записываем в объект file2 также как на экран
			file2 << "Студент номер " << cnt + 1 << endl;
			file2 << "Фамилия: " << buff[cnt].last_name << endl;
			file2 << "Имя: " << buff[cnt].first_name << endl;
			file2 << "Год рождения: " << buff[cnt].year << endl;
			file2 << "Месяц рождения: " << buff[cnt].month << endl;
			file2 << "День рождения: " << buff[cnt].day << endl;
			file2 << "Средний балл: " << buff[cnt].av_grade << endl << endl;
		}
		cout << ("Файл записан\n");
		file2.close();
	}
	system("pause");
}

void input_in_buff() { //ввод данных;
	int k = 1;
	while (k != 0) {
		system("cls");
		cout << "Введите имя\n";
		cin >> buff[i].first_name;
		cout << "Введите фамилию\n";
		cin >> buff[i].last_name;
		cout << "Введите год, месяц и день рождения\n";
		cin >> buff[i].year;
		cin >> buff[i].month;
		cin >> buff[i].day;
		cout << "Введите средний балл\n";
		cin >> buff[i].av_grade;
		i++;
		printf("Введите 0 для завершения записи или любое другое число для продолжения\n");
		cin >> k;
	}
}
void show_all_base(){ // показать всю базу данных
	system("cls"); //очищение экрана
	for (cnt = 0; cnt < i; cnt++) {
		show_student(); //функция показать 1 студента, объявленная выше
	}
	system("pause");
}


void search() { //функция поиска студента по фамилии
	int k = 0;
	string p;
	printf("Введите фамилию\n");
	cin >> p;
	for (cnt = 0; cnt < i; cnt++) { //просматриваем всех студентов
		if (p == buff[cnt].last_name) { //если находим
			system("cls"); // то очищаем экран
			show_student(); // и выводим о нем информацию
			cout << "Если хотите удалить данные о студенте, введите 1 или 2, если хотите посмотреть следующего" << endl;
			cin >> k;
			if (k == 1) {
				while (cnt < i) { //все студенты до того, кого удаляем остаются
					buff[cnt] = buff[cnt + 1]; // а после заменяются на следующего
					cnt++;
				}
				i--; // уменьшаем количество студентов на 1, так как удалили
				cout << "Удаление завершено" << endl;
			}
			if (k == 2) {
				if (cnt + 1 != i) { // проверка не последний ли студент
					system("cls"); //очищение экрана
					cnt++;
					show_student(); //если не последний, то выводим следующего
				}
				else {
					cout << "Это последний студент" << endl;
				}
			}

		}
		else {
			cout << "Студент не найден или больше студентов с такой фамилией нет" << endl;
		}
	}
	system("pause");
}

int main() {
	setlocale(0, "rus");
	SetConsoleCP(1251); //устанавливаем ввод по-русски имен и фамилий
	SetConsoleOutputCP(1251); //не забыть изменить шрифт консоли
	int p = 1;
	while (p != 0){
		system("cls");
		cout << "Меню\n";
		cout << "1 - Добавить данные\n";
		cout << "2 - Запись данных в файл\n";
		cout << "3 - Вывести на экран базу данных\n";
		cout << "4 - Считать базу данных из файла\n";
		cout << "5 - Поиск по фамилии\n";
		cout << "0 - Завершение работы программы\n";
		cin >> p;
		switch (p){
			case 1:input_in_buff(); break;
			case 2:out_put_to_file(); break;
			case 3:show_all_base(); break;
			case 4:reading(); break;
			case 5:search(); break;
			case 0: exit(1); break;
		}
	}
	return 0;
}